﻿using UnityEngine;

namespace Game
{
public class KeyDoor : Key
{
    [SerializeField] private Door _door;
    
    protected override void Interaction(Player player)
    {
        _door.Open();
    }

}
}