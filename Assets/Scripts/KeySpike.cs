﻿using System.Threading;
using UnityEngine;

namespace Game
{
public class KeySpike : Key
{
    private Spike[] _spikes;

    private void Start()
    {
        _spikes = FindObjectsOfType<Spike>();
    }
    
    protected override void Interaction(Player player)
    {
        foreach (var spike in _spikes)
        {
            Destroy(spike.gameObject);
        }
    }

}
}