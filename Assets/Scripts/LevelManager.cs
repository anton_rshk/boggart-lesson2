using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public void Load(string name)
    {
        SceneManager.LoadScene(name);
    }
    
    public void Load(int index)
    {
        SceneManager.LoadScene(index);
    }
    
    public void ReLoad()
    {
        string name = SceneManager.GetActiveScene().name;
        Load(name);
    }
    
    public void NextLevel()
    {
        int index = CheckNextLevel();
        
        if (index == -1) return;
        Load(index);
    }

    public int CheckNextLevel()
    {
        int countScenes = SceneManager.sceneCountInBuildSettings;
        int index = SceneManager.GetActiveScene().buildIndex;
       
        index++;

        if (index >= countScenes) return -1;
        return index;
    }

    public void Exit()
    {
        Application.Quit();
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#endif
    }
    
    
}
