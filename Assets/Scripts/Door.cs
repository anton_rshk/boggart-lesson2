using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

public class Door : Exit
{
    public override void Open()
    {
        base.Open();
        Destroy(gameObject);
    }
}
